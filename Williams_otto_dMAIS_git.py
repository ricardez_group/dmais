# -*- coding: utf-8 -*-
"""
Created on Mon Sep 27 10:25:22 2021

@author: g2patron
"""

from pyomo.environ import * #import pyomo
from pyomo.dae import *  #import pyomo dae extension
from pyomo.opt import SolverFactory  #import solvers
from pyomo.opt import TerminationCondition  #import pyomo solution status
from pyomo.opt import SolverStatus  #import pyomo solver status
import numpy as np #import numpy
import matplotlib.pyplot as plt #import matplotlib
import math
import time
import sys


def williams_otto_RTO_mismatch(nstates, ninputs, epsg, lamg, lamphi, uk): #mismatched steady-state model

    'Declare Model'
    
    m = ConcreteModel()  
    'Declare Model Sets'
    
    m.comp = RangeSet(1,nstates)
    
    'Declare Model Parameters'

    m.W = Param(initialize = 2104.7, mutable = True)    
    m.E1 = Param(initialize = 8077.6, mutable = True)
    m.E2 = Param(initialize = 12438.5, mutable = True)

    m.lam1 = Param(initialize = lamphi[0,0], mutable = True)
    m.lam2 = Param(initialize = lamphi[1,0], mutable = True)
    'Declare Model Variables'
    
    m.X = Var(m.comp,initialize={1:0.06342867804909315,2:0.47602275309129916,3:0.0110995919709198,4:0.1317097224222399,5:0.08147971443631048,6:0.1045498176105465})#, within = NonNegativeReals)
    
    m.Fr = Var(initialize= 7.9)
    m.Fa = Var(initialize = 1.8)
    m.Fb = Var(initialize= 6.1, bounds=(3,6))
    
    m.k1 = Var(initialize=1.1329053586626874e-25)
    m.k2 = Var(initialize=7.96210497709527e-31)
    m.k3 = Var(initialize=3.054039072696644e-40)
    
    m.Tr = Var(initialize=366.05, bounds=(343.15,373.15))
    
    'Declare Model Equations'
    
    'Overall Reactor Mass Balance'
    
    def _reactor_balance(m):
        return m.Fr ==  m.Fa + m.Fb 
    m.reactor_balance = Constraint(rule=_reactor_balance)   
    
    'Component Mass Balances'
    
    def _A_balance(m):
        return 0 == m.Fa - (m.Fr)*m.X[1] - m.k1*m.W*m.X[1]*m.X[2]**2 - m.k2*m.W*m.X[1]*m.X[2]*m.X[6]
    m.A_balance = Constraint(rule=_A_balance)
    
    def _B_balance(m):
        return 0 == m.Fb - (m.Fr)*m.X[2] - 2*m.k1*m.W*m.X[1]*m.X[2]**2 - m.k2*m.W*m.X[1]*m.X[2]*m.X[6]
    m.B_balance = Constraint(rule=_B_balance)
    
    def _E_balance(m):
        return 0 == 2*m.k1*m.W*m.X[1]*m.X[2]**2 - (m.Fr)*m.X[4] 
    m.E_balance = Constraint(rule=_E_balance)
    
    def _G_balance(m):
        return 0 == 3*m.k2*m.W*m.X[1]*m.X[2]*m.X[6] - (m.Fr)*m.X[5] 
    m.E_balance = Constraint(rule=_E_balance)
    
    def _P_balance(m):
        return 0 == m.k1*m.W*m.X[1]*m.X[2]**2 -(m.Fr)*m.X[6] - m.k2*m.W*m.X[1]*m.X[2]*m.X[6]
    m.P_balance = Constraint(rule=_P_balance)
        
    'Reaction Rate Expressions'
    
    def _k1_expression(m):
        return m.k1 == (2.189e8)*exp(-m.E1/m.Tr)
    m.k1_expression = Constraint(rule=_k1_expression)
    
    def _k2_expression(m):
        return m.k2 == (4.310e13)*exp(-m.E2/m.Tr)
    m.k2_expression = Constraint(rule=_k2_expression)

    m.obj = Objective(expr=Price1*m.Fr*m.X[6] + Price2*m.Fr*m.X[4] - 76.23*m.Fa - 114.34*m.Fb + m.lam1*m.Fb + m.lam2*m.Tr, sense=maximize)    
       
    return m

def williams_otto_RTO(nstates, Price1, Price2):  #full steady-state model (not mismatched)
    
    'Declare Model'
    
    m = ConcreteModel()  
    'Declare Model Sets'
    
    m.comp = RangeSet(1,nstates)
    
    'Declare Model Parameters'

    m.W = Param(initialize = 2104.7, mutable = True)    
    m.E1 = Param(initialize = 6666.7, mutable = True)
    m.E2 = Param(initialize = 8333.3, mutable = True)
    m.E3 = Param(initialize = 11111, mutable = True)

    'Declare Model Variables'
    
    m.X = Var(m.comp,initialize={1:0.06342867804909315,2:0.47602275309129916,3:0.0110995919709198,4:0.1317097224222399,5:0.08147971443631048,6:0.1045498176105465})#, within = NonNegativeReals)
    
    m.Fr = Var(initialize= 7.9)
    m.Fa = Var(initialize = 1.8)
    m.Fb = Var(initialize= 6.1, bounds=(3,6))
    
    m.k1 = Var(initialize=1.1329053586626874e-25)
    m.k2 = Var(initialize=7.96210497709527e-31)
    m.k3 = Var(initialize=3.054039072696644e-40)
    
    m.Tr = Var(initialize=366.05, bounds=(343.15,373.15))
    
    'Declare Model Equations'
    
    'Overall Reactor Mass Balance'
    
    def _reactor_balance(m):
        return m.Fr ==  m.Fa + m.Fb 
    m.reactor_balance = Constraint(rule=_reactor_balance)   
    
    'Component Mass Balances'
    
    def _A_balance(m):
        return 0 == m.Fa - (m.Fr)*m.X[1] - m.k1*m.W*m.X[1]*m.X[2]
    m.A_balance = Constraint(rule=_A_balance)
    
    def _B_balance(m):
        return 0 == m.Fb - (m.Fr)*m.X[2] - m.k1*m.W*m.X[1]*m.X[2] - m.k2*m.W*m.X[2]*m.X[3]
    m.B_balance = Constraint(rule=_B_balance)
    
    def _C_balance(m):
        return 0 == - (m.Fr)*m.X[3] + 2*m.k1*m.W*m.X[1]*m.X[2]  - 2*m.k2*m.W*m.X[2]*m.X[3] - m.k3*m.W*m.X[3]*m.X[6]
    m.C_balance = Constraint(rule=_C_balance)
    
    def _E_balance(m):
        return 0 == - (m.Fr)*m.X[4] + m.k2*m.W*m.X[2]*m.X[3] 
    m.E_balance = Constraint(rule=_E_balance)
    
    def _G_balance(m):
        return 0 == - (m.Fr)*m.X[5] + 1.5*m.k3*m.W*m.X[3]*m.X[6] 
    m.G_balance = Constraint(rule=_G_balance)
    
    def _P_balance(m):
        return 0 == -(m.Fr)*m.X[6] + m.k2*m.W*m.X[2]*m.X[3]  - 0.5*m.k3*m.W*m.X[3]*m.X[6] 
    m.P_balance = Constraint(rule=_P_balance)
        
    'Reaction Rate Expressions'
    
    def _k1_expression(m):
        return m.k1 == (1.6599e6)*exp(-m.E1/m.Tr)
    m.k1_expression = Constraint(rule=_k1_expression)
    
    def _k2_expression(m):
        return m.k2 == (7.2117e8)*exp(-m.E2/m.Tr)
    m.k2_expression = Constraint(rule=_k2_expression)
    
    def _k3_expression(m):
        return m.k3 == (2.6745e12)*exp(-m.E3/m.Tr)
    m.k3_expression = Constraint(rule=_k3_expression)

    m.obj = Objective(expr=Price1*m.Fr*m.X[6] + Price2*m.Fr*m.X[4] - 76.23*m.Fa - 114.34*m.Fb, sense=maximize)     
   
    return m

def synthetic_plant(P, nstates, ninputs, nfetp, ncpt):  #full dynamic model to act as plant

    'Declare Model'
    
    m = ConcreteModel()
    
    'Declare Model Sets'
    
    m.t = ContinuousSet(bounds = (0,P))
    m.comp = RangeSet(1,nstates)
    
    'Declare Model Parameters'

    m.W = Param(initialize = 2104.7)
    m.Xinit = Param(m.comp, initialize = {1:0.06342867804909315,2:0.47602275309129916,3:0.0110995919709198,4:0.1317097224222399,5:0.08147971443631048,6:0.1045498176105465}, mutable = True)
    m.E1 = Param(initialize = 6666.7, mutable = True)
    m.E2 = Param(initialize = 8333.3, mutable = True)
    m.E3 = Param(initialize = 11111, mutable = True)
    
    'Declare Model Variables'
    
    m.X = Var(m.comp, m.t, initialize={1:0.06342867804909315,2:0.47602275309129916,3:0.0110995919709198,4:0.1317097224222399,5:0.08147971443631048,6:0.1045498176105465})
    
    m.Fr = Var(m.t, initialize= 7.9)
    m.Fa = Var(m.t, initialize = 1.8)
    m.Fb = Var(m.t, initialize= 6.1, bounds=(2,10))
    
    m.k1 = Var(m.t, initialize=0.016299443791589287)
    m.k2 = Var(m.t, initialize=0.07050847398199092)
    m.k3 = Var(m.t, initialize=0.12048349722125962)
    
    m.Tr = Var(m.t, initialize=366.05, bounds=(323.15,423.15))
    
    m.dXdt = DerivativeVar(m.X, wrt=m.t)
    
    discretizer = TransformationFactory ('dae.collocation')
    discretizer.apply_to(m, nfe=1, wrt=m.t, ncp=ncpt, scheme='LAGRANGE-RADAU')
    
    'Declare Model Equations'
    
    'Overall Reactor Mass Balance'
    
    def _reactor_balance(m,j):
        return m.Fr[j] ==  m.Fa[j] + m.Fb[j]
    m.reactor_balance = Constraint(m.t, rule=_reactor_balance)   

    
    'Component Mass Balances'
    
    def _A_balance(m,j):
        if j == 0:
            return Constraint.Skip
        return m.W*m.dXdt[1,j] == m.Fa[j] - m.Fr[j]*m.X[1,j] - m.k1[j]*m.W*m.X[1,j]*m.X[2,j]
    m.A_balance = Constraint(m.t, rule=_A_balance)
    
    def _A_init(m):
        return m.X[1,0] == m.Xinit[1] 
    m. A_init = Constraint(rule = _A_init)   
    
    def _B_balance(m,j):
        if j == 0:
            return Constraint.Skip
        return m.W*m.dXdt[2,j] == m.Fb[j] - m.Fr[j]*m.X[2,j] - m.k1[j]*m.W*m.X[1,j]*m.X[2,j] - m.k2[j]*m.W*m.X[2,j]*m.X[3,j]
    m.B_balance = Constraint(m.t, rule=_B_balance)
    
    def _B_init(m):
        return m.X[2,0] == m.Xinit[2] 
    m. B_init = Constraint(rule = _B_init)   
        
    def _C_balance(m,j):
        if j == 0:
            return Constraint.Skip
        return m.W*m.dXdt[3,j] == - m.Fr[j]*m.X[3,j] + 2*m.k1[j]*m.W*m.X[1,j]*m.X[2,j]  - 2*m.k2[j]*m.W*m.X[2,j]*m.X[3,j] - m.k3[j]*m.W*m.X[3,j]*m.X[6,j]
    m.C_balance = Constraint(m.t, rule=_C_balance)
    
    def _C_init(m):
        return m.X[3,0] == m.Xinit[3] 
    m. C_init = Constraint(rule = _C_init)   
        
    def _E_balance(m,j):
        if j == 0:
            return Constraint.Skip
        return m.W*m.dXdt[4,j] == - m.Fr[j]*m.X[4,j] + m.k2[j]*m.W*m.X[2,j]*m.X[3,j] 
    m.E_balance = Constraint(m.t, rule=_E_balance)
    
    def _E_init(m):
        return m.X[4,0] == m.Xinit[4] 
    m. E_init = Constraint(rule = _E_init)   
        
    def _G_balance(m,j):
        if j == 0:
            return Constraint.Skip
        return m.W*m.dXdt[5,j] == - m.Fr[j]*m.X[5,j] + 1.5*m.k3[j]*m.W*m.X[3,j]*m.X[6,j] 
    m.G_balance = Constraint(m.t, rule=_G_balance)
    
    def _G_init(m):
        return m.X[5,0] == m.Xinit[5] 
    m. G_init = Constraint(rule = _G_init)   
        
    def _P_balance(m,j):
        if j == 0:
            return Constraint.Skip
        return m.W*m.dXdt[6,j] == - m.Fr[j]*m.X[6,j] + m.k2[j]*m.W*m.X[2,j]*m.X[3,j]  - 0.5*m.k3[j]*m.W*m.X[3,j]*m.X[6,j]
    m.P_balance = Constraint(m.t, rule=_P_balance)
    
    def _P_init(m):
        return m.X[6,0] == m.Xinit[6] 
    m. P_init = Constraint(rule = _P_init)   
        
    'Reaction Rate Expressions'
    
    def _k1_expression(m,j):
        return 0 == m.k1[j] - (1.6599e6)*exp(-m.E1/m.Tr[j])
    m.k1_expression = Constraint(m.t, rule=_k1_expression)
    
    def _k2_expression(m,j):
        return 0 == m.k2[j] - (7.2117e8)*exp(-m.E2/m.Tr[j])
    m.k2_expression = Constraint(m.t, rule=_k2_expression)
    
    def _k3_expression(m,j):
        return 0 == m.k3[j] - (2.6745e12)*exp(-m.E3/m.Tr[j])
    m.k3_expression = Constraint(m.t, rule=_k3_expression)
       
    return m

'Process'    
nstates = 6 #number of states
ninputs = 2 #number of states
adapt = [0,1] #start witj both inputs are adapted before choosing 
ind1 = 0 #indicates that inputs were just changed
ind2 = 1 #indicates that disturbance just occurred
dis = 1.8 #nominal disturbance value

'Simulation'
nfetp = 10 #number of finite elements per time step for dynamic plant model
ncpt = 3 #number of collocation points per finite element for dynamic plant model
P = 180 #plant time step (s)
opw = 12000 #total number of time steps in given simulation

'Modifier adaptation'
li = 10 #settling time when initializing simulation (time periods)
lio = 50 #settling time after set point change (time periods)
perti = np.zeros([ninputs,1])
sst = 50 #settling time after perturbation (time periods)
nconst = 0 #number of constrains (0 for W-O)
count = 0 #keeps track of how many perturbations have been performed
pert =  0.001 #perturbations size
period = 300 #disturbance period (time periods)

Price1 = 1143.38 #product price 1 
Price2 = 25.92 #product price 2

switch = [] #indicates which input is being used for adaptation
pricespred = [] #modified cost

filtphi = [0.01,0.01] #cost gradient modifier filters
filtg = [0.01,0.01] #constraint gradient modifier filters
filte = [0.01,0.01] #constraint value modifier filters

t = [] #time vector


Urto = np.zeros([ninputs,1]) #current inputs for non-mismatched model
Urto[0,0] = 6.1 #nominal inlet flowrate of b
Urto[1,0] = 366.5 #nominal reactor temperature
Uma = np.zeros([ninputs,1]) #current inputs for mismatched model
Uma[0,0] = 6.1
Uma[1,0] = 366.5

'Arrays to store data. Those denominated "g" are for constraint and are not being used in this case study.'

x1 = np.array([[0] for i in range(0,nstates)], dtype='float64')
x2 = np.array([[0] for i in range(0,nstates)], dtype='float64')
u1 = np.array([[0] for i in range(0,ninputs)], dtype='float64')
u2 = np.array([[0] for i in range(0,ninputs)], dtype='float64')
econ1 = []
econ2 = []
R = np.array([[0] for i in range(0,ninputs)], dtype='float64')
Rphi = np.array([[0] for i in range(0,ninputs)], dtype='float64')

lamg = np.zeros([ninputs, nconst])
lamgp = np.zeros([ninputs, nconst])
lamphi = np.zeros([ninputs, 1])
lamphip = np.zeros([ninputs, 1])
lamphiperm = np.zeros([ninputs, 1])
lamphidummy= np.zeros([ninputs, 1])
epsg = np.zeros([nconst, 1])
epsgp = np.zeros([nconst, 1])
dgdup = np.zeros([ninputs, nconst])
dgdum = np.zeros([ninputs, nconst])
gpold = np.zeros([nconst, 1])
gmold = np.zeros([nconst, 1])
uk = np.zeros([ninputs,1])
gpnew = np.zeros([ninputs, nconst])
phinextp = np.zeros([ninputs,1])
phinextm = np.zeros([ninputs,1])
gmnew = np.zeros([ninputs, nconst])
dphidum = np.zeros([ninputs,1])
dphidup = np.zeros([ninputs,1])
grad = np.zeros([ninputs,1])

lambdaphit = np.array([[0] for i in range(0,ninputs)], dtype='float64')
lambdagt = np.array([[0] for i in range(0,nconst)], dtype='float64')
gradphit = np.array([[0] for i in range(0,ninputs)], dtype='float64')
gradgt = np.array([[0] for i in range(0,nconst)], dtype='float64')

'Warm-starting RTO and MA using feasibility problems'

rto = williams_otto_RTO(nstates, Price1, Price2)
rto.Fa.fix(1.8)
rto.Fb.fix(6.2)
rto.Tr.fix(366.05)
opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
opt.options['linear_solver'] = 'ma27'
rtoresults = opt.solve(rto,tee=True)
rto.solutions.store_to(rtoresults)

ma = williams_otto_RTO_mismatch(nstates, ninputs, epsg, lamg, lamphi, uk)
ma.Fa.fix(1.8)
rto.Fb.fix(6.2)
rto.Tr.fix(366.05)
opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
opt.options['linear_solver'] = 'ma27'
maresults = opt.solve(ma,tee=True)
ma.solutions.store_to(maresults)


plant1 = synthetic_plant(P, nstates, ninputs, nfetp, ncpt) #name for plant 1 optimized with no mismatch
plant2 = synthetic_plant(P, nstates, ninputs, nfetp, ncpt) #name for plant 2 optimized with mismatch 

'Disturbance sequence used to compare different flilter values'

disturbance = [2.2960659635860456,
 1.152374034741192,
 2.9428389150082293,
 1.440122470974553,
 3.1919151760152014,
 4.088120096744708,
 4.287331290116197,
 1.3594667750619227,
 5.229493260659365,
 0.8505684526085093,
 3.282277831748809,
 1.235995170674364,
 0.7098911265135575,
 5.187266747995826,
 4.802731748040058,
 2.2960191299536428,
 2.6258699520946203,
 3.8858695221993145,
 3.108687549820443,
 5.323734654983311,
 4.782112359003806,
 1.4182360772893654,
 5.099327954737779,
 1.3251358710761487,
 4.641952456984118,
 3.261970282363131,
 1.7127716414468905,
 5.139124026462015,
 1.5664986530049148,
 4.48884678741824,
 4.638990321738349,
 4.0656907666453,
 2.831425032960049,
 1.064492719963879,
 3.629964919397012,
 1.998616608259975,
 1.2163137411677407,
 1.9664322762890465,
 3.396888536302847]

for l in range(0,opw): #loop for opw time periods

    if ind1 == 1 and ind2 == 1: #choosing which input to use for modification
        pred = np.zeros([ninputs,nconst+1])
        predtot = []
        for i in range(0,len(lamphi)):
            lamphi[i] = value(lamphip[i])
            lamphiperm[i] = value(lamphip[i]) 

            ma = williams_otto_RTO_mismatch(nstates, ninputs, epsg, lamg, lamphi, uk)  
            ma.Fa.fix(dis)
            ma.Fb.fix(value(uk[0,0]))
            ma.Tr.fix(value(uk[1,0])) 
            opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
            maresults = opt.solve(ma,tee=True)
            ma.solutions.store_to(maresults)
            pred[i,0] = value((Price1*ma.Fr*ma.X[6] + Price2*ma.Fr*ma.X[4] - 76.23*ma.Fa - 114.34*ma.Fb + ma.lam1*ma.Fb + ma.lam2*ma.Tr ))#- econ2[-1])/econ2[-1])            
            lamphi = np.zeros([ninputs, 1])
            predtot.append(sum(abs(pred[i,j]) for j in range(0,len(pred[0]))))
                
        adapt = [predtot.index(max(predtot))]
        
        for i in range(0,len(lamphi)):
            if i == adapt[0]:
                pass
            else: 
                grad[i,0] = 0
                dphidum[i,0] = 0
                dphidup[i,0] = 0
                lamphip[i,0] = 0
                for j in range(0,len(lamg[0])):
                    lamgp[i,j] = 0
                    dgdup[i,j] = 0
                    dgdum[i,j] = 0
        ind1 = 0
        ind2 = 0
        switch.append(predtot.index(max(predtot)))   

    if (l/period).is_integer() == True and l > 0: # if disturbance has just occured
        li = l + 50 #settling time
        for i in adapt: #set modifiers to 0
            lamphip[i,0] = value(0)
            grad[i,0] = value(0)
            dphidup[i,0] = value(0)
            dphidum[i,0] = value(0)
            for j in range(0,len(lamgp[0])):
                lamgp[i,j] = value(0)
                dgdup[i,j] = 0
                dgdum[i,j] = 0
        adapt = [0,1] # return to full MA scheme
        ind1 = 0
        ind2 = 1
        dis = disturbance[int(l/period)-1]
#        dis = 1.8*np.random.uniform (0.3,3) # can generate random disturbances if so desired by sampling from this distribution
#        disturbance.append(dis)
        
        plant1 = synthetic_plant(P, nstates, ninputs, nfetp, ncpt)
        plant2 = synthetic_plant(P, nstates, ninputs, nfetp, ncpt)  
    else:
        pass
        
    if l == 0: #setting nominal input values and initial conditions at t == 0
        for i in plant1.comp:
            plant1.Xinit[i] = value(rto.X[i])
        plant1.Fb.fix(value(6.1))
        plant1.Tr.fix(value(366.5))
        plant1.Fa.fix(dis)
        
        for i in plant2.comp:
            plant2.Xinit[i] = value(rto.X[i])
        plant2.Fb.fix(value(6.1))
        plant2.Tr.fix(value(366.5))
        plant2.Fa.fix(dis)
        
    else: #setting updated input values and initial conditions  thereafter
        for i in plant1.comp:
            plant1.Xinit[i] = value(x1[i-1,-2])        
        plant1.Fb.fix(value(Urto[0,0]))
        plant1.Tr.fix(value(Urto[1,0]))
        plant1.Fa.fix(dis)

        for i in plant2.comp:
            plant2.Xinit[i] = value(x2[i-1,-2])        
        plant2.Fa.fix(dis)
        
        if l == li: #sets perturbation count to 0
            count = 0
                
        if l == (li + sst*count + 1) and l < (li + sst*len(adapt)):  #indicates which input is being perturbed
            for i in adapt:
                if i == adapt[count]:
                    perti[i] = value(pert)
                else:
                    perti[i] = value(0)

        if count > len(adapt)-1:
            plant2.Fb.fix(value(Uma[0,0]))
            plant2.Tr.fix(value(Uma[1,0]))                
            count = 0                   
        elif l > (li + sst*(count)) and l < (li + sst*(count+1)):
            plant2.Fb.fix(value(Uma[0,0])+perti[0,0]*6.1)
            plant2.Tr.fix(value(Uma[1,0])+perti[1,0]*366.5)                    
        elif l == (li + sst*(count+1)):
            count += 1
#        elif l > (li + sst*(count+1)):
#            for j in plant2.inputs:
#                plant2.u[j].fix(value(Uma[j-1,0]))
        else:
            plant2.Fb.fix(value(Uma[0,0]))
            plant2.Tr.fix(value(Uma[1,0]))  
                     
    opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt") # simulating plant 1 as dynamic feasibility problem
    opt.options['linear_solver'] = 'ma27'
    plantresults1 = opt.solve(plant1,tee=True)
    plant1.solutions.store_to(plantresults1)
    if plantresults1.solver.termination_condition != TerminationCondition.optimal:
        break

    opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt") # simulating plant 2 as dynamic feasibility problem
    opt.options['linear_solver'] = 'ma27'
    plantresults2 = opt.solve(plant2,tee=True)
    plant2.solutions.store_to(plantresults2)
    if plantresults2.solver.termination_condition != TerminationCondition.optimal:
        break
    
    if l == li + len(adapt)*sst + 1:  #solving non-mismatched RTO at every disturbance 
        rto = williams_otto_RTO(nstates, Price1, Price2)
        rto.Fa.fix(dis)
        opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
        opt.options['linear_solver'] = 'ma27'
        rtoresults = opt.solve(rto,tee=True)
        rto.solutions.store_to(rtoresults)
        Urto[0] = value(rto.Fb)
        Urto[1] = value(rto.Tr)

    if l == li + len(adapt)*sst + 1: #updating modified cost and input used for modification
        for i in range(0,len(lamphi)):
            lamphi[i] = value((1-filtphi[i])*grad[i] + filtphi[i]*lamphip[i])
        for i in range(0,len(lamg)):
            for j in range(0,len(lamg[0])):
                lamg[i,j] = value((1-filtg[i])*(dgdup[i,j] - dgdum[i,j]) + filtg[i]*lamgp[i,j])
        for i in range(0,len(epsg)):
            epsg[i] = value((1-filte[0])*(gpold[i] - gmold[i]) + filte[0]*epsgp[i])

        for i in range(0,len(lamphi)):
            lamphip[i] = value(lamphi[i])
        for i in range(0,len(lamg)):
            for j in range(0,len(lamg[0])):
                lamgp[i,j] = value(lamg[i,j])
        for i in range(0,len(epsg)):
            epsgp[i] = value(epsg[i])                

        for i in range(0,len(uk)):
            uk[i] = value(Uma[i,0])
            
        lamphiperm[adapt[0]] = value(lamphi[adapt[0]])           
        for i in range(0,len(lamphi)):
            lamphidummy[i,0] = value(lamphiperm[i,0])
            ma = williams_otto_RTO_mismatch(nstates, ninputs, epsg, lamg, lamphidummy, uk)  
            ma.Fa.fix(dis)
            ma.Fb.fix(value(uk[0,0]))
            ma.Tr.fix(value(uk[1,0])) 
            opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
            maresults = opt.solve(ma,tee=True)
            ma.solutions.store_to(maresults)
            pricespred.append(value(Price1*ma.Fr*ma.X[6] + Price2*ma.Fr*ma.X[4] - 76.23*ma.Fa - 114.34*ma.Fb + ma.lam1*ma.Fb + ma.lam2*ma.Tr))# - econ2[-1])/econ2[-1]))                
            lamphidummy = np.zeros([ninputs, 1])

        switch.append(pricespred.index(max(pricespred))) 
        adapt = [pricespred.index(max(pricespred))]
        for i in range(0,len(lamphi)):
            if i == adapt[0]:
                pass
            else: 
                grad[i,0] = 0
                dphidum[i,0] = 0
                dphidup[i,0] = 0
                lamphip[i,0] = 0
                for j in range(0,len(lamg[0])):
                    lamgp[i,j] = 0
                    dgdup[i,j] = 0
                    dgdum[i,j] = 0

        for i in range(0,len(lamphi)): #updating modifiers
            lamphi[i] = value((1-filtphi[i])*grad[i] + filtphi[i]*lamphip[i])
        for i in range(0,len(lamg)):
            for j in range(0,len(lamg[0])):
                lamg[i,j] = value((1-filtg[i])*(dgdup[i,j] - dgdum[i,j]) + filtg[i]*lamgp[i,j])
        for i in range(0,len(epsg)):
            epsg[i] = value((1-filte[0])*(gpold[i] - gmold[i]) + filte[0]*epsgp[i])

        for i in range(0,len(lamphi)):
            lamphip[i] = value(lamphi[i])
        for i in range(0,len(lamg)):
            for j in range(0,len(lamg[0])):
                lamgp[i,j] = value(lamg[i,j])
        for i in range(0,len(epsg)):
            epsgp[i] = value(epsg[i])                

        for i in range(0,len(uk)):
            uk[i] = value(Uma[i,0])
                    
        ma = williams_otto_RTO_mismatch(nstates, ninputs, epsg, lamg, lamphi, uk)  
        ma.Fa.fix(dis)
        opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt") #Solving dMAIS problem at every refinement step
        opt.options['linear_solver'] = 'ma27'
        maresults = opt.solve(ma,tee=True)
        ma.solutions.store_to(maresults)
        cost1 = value(Price1*ma.Fr*ma.X[6] + Price2*ma.Fr*ma.X[4] - 76.23*ma.Fa - 114.34*ma.Fb)
        if (maresults.solver.termination_condition == TerminationCondition.optimal):
            Uma[0,0] = value(ma.Fb)
            Uma[1,0] = value(ma.Tr)

        lamg = np.zeros([ninputs, nconst])
        lamphi = np.zeros([ninputs, 1])
        epsg = np.zeros([nconst, 1])
        pricespred = []     
        
        li = l + lio
        ind1 = 1
        
    if l == li - 1: #saving current mismatched model cost prediction
        phipold = value(econ2[-1])
        ma = williams_otto_RTO_mismatch(nstates, ninputs, epsg, lamg, lamphi, uk)  
        ma.Fa.fix(dis)
        ma.Fb.fix(value(Uma[0,0]))
        ma.Tr.fix(value(Uma[1,0])) 
        opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
        maresults = opt.solve(ma,tee=True)
        ma.solutions.store_to(maresults)
        if (maresults.solver.termination_condition == TerminationCondition.optimal):
            pass
        else:
            break
        phimold = value(Price1*ma.Fr*ma.X[6] + Price2*ma.Fr*ma.X[4] - 76.23*ma.Fa - 114.34*ma.Fb)      

    elif (li <= l) and (l <= li +sst*len(adapt)):  #performing perturbations on mismatched model

        if count > len(adapt)-1:
            pass                 
        elif l == (li + sst*(count+1) - 2):
            phinextp[adapt[count]] =value(Price1*plant2.Fr[P]*plant2.X[6,P] + Price2*plant2.Fr[P]*plant2.X[4,P] - 76.23*plant2.Fa[P] - 114.34*plant2.Fb[P])  
            ma = williams_otto_RTO_mismatch(nstates, ninputs, epsg, lamg, lamphi, uk)  
            ma.Fa.fix(dis)
            ma.Fb.fix(value(plant2.Fb[P]))
            ma.Tr.fix(value(plant2.Tr[P])) 
            opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
            maresults = opt.solve(ma,tee=True)
            ma.solutions.store_to(maresults)
            if (maresults.solver.termination_condition == TerminationCondition.optimal):
                pass
            else:
                break
            phinextm[adapt[count],0] = value(Price1*ma.Fr*ma.X[6] + Price2*ma.Fr*ma.X[4] - 76.23*ma.Fa - 114.34*ma.Fb)   
            if count == 0:
                dphidum[adapt[count],0] = value((phinextm[adapt[count],0]-phimold)/(pert*6.1))
                dphidup[adapt[count],0] = value((phinextp[adapt[count],0]-phipold)/(pert*6.1))
            if count == 1:
                dphidum[adapt[count],0] = value((phinextm[adapt[count],0]-phimold)/(pert*366.5))
                dphidup[adapt[count],0] = value((phinextp[adapt[count],0]-phipold)/(pert*366.5))
            grad[adapt[count],0] = value(dphidup[adapt[count],0]-dphidum[adapt[count],0])
            v = []
            for i in range(0,ninputs):
                v.append(value(grad[i,0]))
            lambdaphit = np.insert(lambdaphit, -1, v, 1)
            v = []
            for i in range(0,ninputs):
                v.append(value(dphidup[i,0]))
            gradphit = np.insert(gradphit, -1, v, 1)          
        else:
            pass
                                  
    else:
        pass
    
    if l == 0: #saving data to arrays created above
        for j in sorted(plant1.t):
            t.append(l*P + j)
            econ1.append(value(Price1*plant1.Fr[j]*plant1.X[6,j] + Price2*plant1.Fr[j]*plant1.X[4,j] - 76.23*plant1.Fa[j] - 114.34*plant1.Fb[j]))
            v = []
            for i in plant1.comp:
                v.append(value(plant1.X[i,j]))
            x1 = np.insert(x1, -1, v, 1)
            v = []                
            v.append(value(plant1.Fb[P]))
            v.append(value(plant1.Tr[P]))            
            u1 = np.insert(u1, -1, v, 1)
            
            econ2.append(value(Price1*plant2.Fr[j]*plant2.X[6,j] + Price2*plant2.Fr[j]*plant2.X[4,j] - 76.23*plant2.Fa[j] - 114.34*plant2.Fb[j]))
            v = []
            for i in plant2.comp:
                v.append(value(plant2.X[i,j]))
            x2 = np.insert(x2, -1, v, 1)
            v = []
            v.append(value(plant2.Fb[P]))
            v.append(value(plant2.Tr[P]))  
            u2 = np.insert(u2, -1, v, 1)
            v = []
            for i in range(0,ninputs):
                v.append(value(abs(lamphip[i]))[0])
            R = np.insert(R, -1, v, 1)           
            v = []
            v.append(value((abs(lamphip[0])))[0])
            v.append(value((abs(lamphip[1])))[0])
            Rphi = np.insert(Rphi, -1, v, 1)  
    else:
        for j in sorted(plant1.t)[1:]:
            t.append(l*P + j)
            econ1.append(value(Price1*plant1.Fr[j]*plant1.X[6,j] + Price2*plant1.Fr[j]*plant1.X[4,j] - 76.23*plant1.Fa[j] - 114.34*plant1.Fb[j]))
            v = []
            for i in plant1.comp:
                v.append(value(plant1.X[i,j]))
            x1 = np.insert(x1, -1, v, 1)
            v = []
            v.append(value(plant1.Fb[P]))
            v.append(value(plant1.Tr[P]))  
            u1 = np.insert(u1, -1, v, 1)
            
            econ2.append(value(Price1*plant2.Fr[j]*plant2.X[6,j] + Price2*plant2.Fr[j]*plant2.X[4,j] - 76.23*plant2.Fa[j] - 114.34*plant2.Fb[j]))
            v = []
            for i in plant2.comp:
                v.append(value(plant2.X[i,j]))
            x2 = np.insert(x2, -1, v, 1)
            v = []
            v.append(value(plant2.Fb[P]))
            v.append(value(plant2.Tr[P]))  
            u2 = np.insert(u2, -1, v, 1)
            v = []
            for i in range(0,ninputs):
                v.append(value(abs(lamphip[i]))[0])
            R = np.insert(R, -1, v, 1)           
            v = []
            v.append(value((abs(lamphip[0])))[0])
            v.append(value((abs(lamphip[1])))[0])
            Rphi = np.insert(Rphi, -1, v, 1)   

    print(l)
            
fig = plt.figure(5) #plot states
for i in plant1.comp:
    plt.plot(t,x1[i-1][:-1], label = str(i))
for i in plant1.comp:
    plt.plot(t,x2[i-1][:-1], label = str(i))
plt.title(r'State')
plt.legend(loc='upper right',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

fig = plt.figure(5) #plot input 1
plt.plot(t,u1[0][:-1], label = str(i))
plt.plot(t,u2[0][:-1], label = str(i))
plt.title(r'Input')
plt.legend(loc='upper right',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

fig = plt.figure(5) #plot input 2
plt.plot(t,u1[1][:-1], label = str(i))
plt.plot(t,u2[1][:-1], label = str(i))
plt.title(r'Input')
plt.legend(loc='upper right',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

fig = plt.figure(5) #plot cost
plt.plot(t,econ1, label = 'RTO (perfect model)')
plt.plot(t,econ2, label = 'MA (full adaptation)')
plt.title(r'Economics')
plt.legend(loc='upper right',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

fig = plt.figure(5) #plot modifiers
plt.plot(t,lamphit[0][:-1], label = str(1))
plt.plot(t,lamphit[1][:-1], label = str(2))
#plt.ylim((-1,500))
plt.title(r'R')
plt.legend(loc='upper right',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()
    

