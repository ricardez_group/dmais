# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 08:40:36 2020

@author: g2patron
"""

from pyomo.environ import * #import pyomo
from pyomo.dae import *  #import pyomo dae extension
from pyomo.opt import SolverFactory  #import solvers
from pyomo.opt import TerminationCondition  #import pyomo solution status
from pyomo.opt import SolverStatus  #import pyomo solver status
import numpy as np #import numpy
import matplotlib.pyplot as plt #import matplotlib
import math
import time
import sys

def evaporator_RTO_no_mismatch(P1,P2,P3,P4): #full steady-state model (not mismatched)
    
    'Declare Model'
    
    m = ConcreteModel()
    m.dual = Suffix(direction=Suffix.IMPORT)    
    
    'Declare Model Parameters'

    m.M = Param(initialize = 20, mutable = True)
    m.C = Param(initialize = 4, mutable = True)
    m.UA2 = Param(initialize = 6.84, mutable = True)
    m.Cp = Param(initialize = 0.07, mutable = True) 
    m.lam = Param(initialize = 38.5, mutable = True)
    m.lams  = Param(initialize = 36.6, mutable = True)      

    'Declare Model Variables'

    m.P2 = Var(initialize = 50.57, bounds = (40,80)) 
    m.P100 = Var(initialize = 193.45, bounds = (10,400)) 
    m.F2 = Var(initialize = 2)    

    m.X1 = Var(initialize = 5) 
    m.X2 = Var(initialize = 25, bounds = (25,100)) 

    m.F1 = Var(initialize = 10)
    m.F3 = Var(initialize = 50, bounds = (1,100))    
    m.F4 = Var()    
    m.F5 = Var()    
    m.F100 = Var()    
    m.F200 = Var(initialize = 207.33, bounds = (10,400))    

    m.T1 = Var(initialize = 40)  
    m.T2 = Var()  
    m.T3 = Var()    
    m.T100 = Var() 
    m.T200 = Var(initialize = 25) 

    m.Q100 = Var()
    m.Q200 = Var()    
    m.UA = Var()
    
    'Declare Model Equations'
    
    'Evaporator Mass Balances'
    
    def _material_balance_l(m):
        return 0 ==  m.F1*m.X1 - m.F2*m.X2
    m.material_balance_l = Constraint(rule=_material_balance_l)   

    def _material_balance_g(m):
        return 0 ==  m.F4 - m.F5
    m.material_balance_g = Constraint(rule=_material_balance_g)  

    'Evaporator Energy Balances'
    
    def _E_energy_balance_1(m):
        return m.T2 ==  0.5616*m.P2 + 0.3126*m.X2 + 48.43
    m.E_energy_balances_1 = Constraint(rule=_E_energy_balance_1)   

    def _E_energy_balance_2(m):
        return m.T3 ==  0.507*m.P2 + 55
    m.E_energy_balances_2 = Constraint(rule=_E_energy_balance_2)  

    def _E_energy_balance_3(m):
        return m.F4 ==  (m.Q100 - m.F1*m.Cp*(m.T2 - m.T1))/m.lam
    m.E_energy_balances_3 = Constraint(rule=_E_energy_balance_3)  
    
    'Heat Jacket Energy Balance'

    def _J_energy_balance_1(m):
        return m.T100 ==  0.1538*m.P100 + 90
    m.J_energy_balances_1 = Constraint(rule=_J_energy_balance_1)   

    def _J_energy_balance_2(m):
        return m.Q100 ==  m.UA*(m.T100 - m.T2)
    m.J_energy_balances_2 = Constraint(rule=_J_energy_balance_2)  

    def _J_energy_balance_3(m):
        return m.UA ==  0.16*(m.F1 + m.F3)
    m.J_energy_balances_3 = Constraint(rule=_J_energy_balance_3)  

    def _J_energy_balance_4(m):
        return m.F100 ==  m.Q100/m.lams
    m.J_energy_balances_4 = Constraint(rule=_J_energy_balance_4)  

    'Condenser Energy Balance'

    def _C_energy_balance_1(m):
        return m.Q200 == m.UA2*(m.T3 - m.T200)/(1 + (m.UA2/(2*m.Cp*m.F200)))
    m.C_energy_balances_1 = Constraint(rule=_C_energy_balance_1)   

    def _C_energy_balance_2(m):
        return m.F5 ==  m.Q200/m.lam
    m.C_energy_balances_2 = Constraint(rule=_C_energy_balance_2)  
# 
    'Overall level'

    def _L_balance(m):
        return m.F2 == m.F1 - m.F4
    m.L_balance_1 = Constraint(rule=_L_balance)   

    m.obj = Objective(expr = P1*(m.F2 + m.F3) + P2*m.F100 + P3*m.F200 + P4*m.P100)
         
    return m

def evaporator_RTO_mismatch(P1,P2,P3,P4, ninputs, epsg, lamg, lamphi, uk):  #mismatched steady-state model
    
    'Declare Model'
    
    m = ConcreteModel()
    m.dual = Suffix(direction=Suffix.IMPORT)    
    m.inputs = RangeSet(0,ninputs)
 
    'Declare Model Parameters'

    m.M = Param(initialize = 20, mutable = True)
    m.C = Param(initialize = 4, mutable = True)
    m.UA2 = Param(initialize = 6.84, mutable = True)
    m.Cp = Param(initialize = 0.07, mutable = True) 
    m.lam = Param(initialize = 35.5, mutable = True)
    m.lams  = Param(initialize = 34.6, mutable = True)      

    'Declare Model Variables'

    m.P2 = Var(initialize = 50.57) 
    m.P100 = Var(initialize = 193.45, bounds = (10,400)) 
    m.F2 = Var(initialize = 2)    

    m.X1 = Var(initialize = 5) 
    m.X2 = Var(initialize = 25) 

    m.F1 = Var(initialize = 10)
    m.F3 = Var(initialize = 50, bounds = (1,100))    
    m.F4 = Var()    
    m.F5 = Var()    
    m.F100 = Var()    
    m.F200 = Var(initialize = 207.33, bounds = (10,400))    

    m.T1 = Var(initialize = 40)  
    m.T2 = Var()  
    m.T3 = Var()    
    m.T100 = Var() 
    m.T200 = Var(initialize = 25) 

    m.Q100 = Var()
    m.Q200 = Var()    
    m.UA = Var()
    
    'Declare Model Equations'
    
    'Evaporator Mass Balances'
    
    def _material_balance_l(m):
        return 0 ==  m.F1*m.X1 - m.F2*m.X2
    m.material_balance_l = Constraint(rule=_material_balance_l)   

    def _material_balance_g(m):
        return 0 ==  m.F4 - m.F5
    m.material_balance_g = Constraint(rule=_material_balance_g)  

    'Evaporator Energy Balances'
    
    def _E_energy_balance_1(m):
        return m.T2 ==  0.5616*m.P2 + 0.3126*m.X2 + 48.43#0.5616*m.P2 + 48.43
    m.E_energy_balances_1 = Constraint(rule=_E_energy_balance_1)   

    def _E_energy_balance_2(m):
        return m.T3 ==  0.507*m.P2 + 55
    m.E_energy_balances_2 = Constraint(rule=_E_energy_balance_2)  

    def _E_energy_balance_3(m):
        return m.F4 ==  (m.Q100 - m.F1*m.Cp*(m.T2 - m.T1))/m.lam
    m.E_energy_balances_3 = Constraint(rule=_E_energy_balance_3)  
    
    'Heat Jacket Energy Balance'

    def _J_energy_balance_1(m):
        return m.T100 ==  0.1538*m.P100 + 90
    m.J_energy_balances_1 = Constraint(rule=_J_energy_balance_1)   

    def _J_energy_balance_2(m):
        return m.Q100 ==  m.UA*(m.T100 - m.T2)
    m.J_energy_balances_2 = Constraint(rule=_J_energy_balance_2)  

    def _J_energy_balance_3(m):
        return m.UA ==  0.16*(m.F3)
    m.J_energy_balances_3 = Constraint(rule=_J_energy_balance_3)  

    def _J_energy_balance_4(m):
        return m.F100 ==  m.Q100/m.lams
    m.J_energy_balances_4 = Constraint(rule=_J_energy_balance_4)  

    'Condenser Energy Balance'

    def _C_energy_balance_1(m):
        return m.Q200 == m.UA2*(m.T3 - m.T200)/(1 + (m.UA2/(2*m.Cp*m.F200)))
    m.C_energy_balances_1 = Constraint(rule=_C_energy_balance_1)   

    def _C_energy_balance_2(m):
        return m.F5 ==  m.Q200/m.lam
    m.C_energy_balances_2 = Constraint(rule=_C_energy_balance_2)  
# 
    'Overall level'

    def _L_balance(m):
        return m.F2 == m.F1 - m.F4
    m.L_balance_1 = Constraint(rule=_L_balance)   

    'Overall level'

    def _X2_lb(m):
        return 25 - m.X2 + float(epsg[0,0]) + float(lamg[0,0])*(m.P100 - float(uk[0,0])) + float(lamg[1,0])*(m.F200 - float(uk[1,0])) + float(lamg[2,0])*(m.F3 - float(uk[2,0])) <= 0
    m.X2_lb = Constraint(rule=_X2_lb) 

    def _P2_lb(m):
        return 40 - m.P2 + float(epsg[1,0]) + float(lamg[0,1])*(m.P100 - float(uk[0,0])) + float(lamg[1,1])*(m.F200 - float(uk[1,0])) + float(lamg[2,1])*(m.F3 - float(uk[2,0])) <= 0
    m.P2_lb = Constraint(rule=_P2_lb) 

    def _P2_ub(m):
        return m.P2 - 100 + float(epsg[2,0]) + float(lamg[0,2])*(m.P100 - float(uk[0,0])) + float(lamg[1,2])*(m.F200 - float(uk[1,0])) + float(lamg[2,2])*(m.F3 - float(uk[2,0])) <= 0
    m.P2_ub = Constraint(rule=_P2_ub) 
    
    m.obj = Objective(expr = P1*(m.F2 + m.F3) + P2*m.F100 + P3*m.F200 + P4*m.P100 + float(lamphi[0,0])*m.P100 + float(lamphi[1,0])*m.F200 + float(lamphi[2,0])*m.F3)
         
    return m

def evaporator_RTO_mismatch_eval(P1,P2,P3,P4, ninputs, epsg, lamg, lamphi, uk): #steady-state mismatched model used for funcion evalutions (e.g., perturbations)
    
    'Declare Model'
    
    m = ConcreteModel()
    m.dual = Suffix(direction=Suffix.IMPORT)    
    m.inputs = RangeSet(0,ninputs)
 
    'Declare Model Parameters'

    m.M = Param(initialize = 20, mutable = True)
    m.C = Param(initialize = 4, mutable = True)
    m.UA2 = Param(initialize = 6.84, mutable = True)
    m.Cp = Param(initialize = 0.07, mutable = True) 
    m.lam = Param(initialize = 35.5, mutable = True)
    m.lams  = Param(initialize = 34.6, mutable = True)      

    'Declare Model Variables'

    m.P2 = Var(initialize = 50.57) 
    m.P100 = Var(initialize = 193.45) 
    m.F2 = Var(initialize = 2)    

    m.X1 = Var(initialize = 5) 
    m.X2 = Var(initialize = 25) 

    m.F1 = Var(initialize = 10)
    m.F3 = Var(initialize = 50)    
    m.F4 = Var()    
    m.F5 = Var()    
    m.F100 = Var()    
    m.F200 = Var(initialize = 207.33)    

    m.T1 = Var(initialize = 40)  
    m.T2 = Var()  
    m.T3 = Var()    
    m.T100 = Var() 
    m.T200 = Var(initialize = 25) 

    m.Q100 = Var()
    m.Q200 = Var()    
    m.UA = Var()
    
    'Declare Model Equations'
    
    'Evaporator Mass Balances'
    
    def _material_balance_l(m):
        return 0 ==  m.F1*m.X1 - m.F2*m.X2
    m.material_balance_l = Constraint(rule=_material_balance_l)   

    def _material_balance_g(m):
        return 0 ==  m.F4 - m.F5
    m.material_balance_g = Constraint(rule=_material_balance_g)  

    'Evaporator Energy Balances'
    
    def _E_energy_balance_1(m):
        return m.T2 ==  0.5616*m.P2 + 0.3126*m.X2 + 48.43#0.5616*m.P2 + 48.43
    m.E_energy_balances_1 = Constraint(rule=_E_energy_balance_1)   

    def _E_energy_balance_2(m):
        return m.T3 ==  0.507*m.P2 + 55
    m.E_energy_balances_2 = Constraint(rule=_E_energy_balance_2)  

    def _E_energy_balance_3(m):
        return m.F4 ==  (m.Q100 - m.F1*m.Cp*(m.T2 - m.T1))/m.lam
    m.E_energy_balances_3 = Constraint(rule=_E_energy_balance_3)  
    
    'Heat Jacket Energy Balance'

    def _J_energy_balance_1(m):
        return m.T100 ==  0.1538*m.P100 + 90
    m.J_energy_balances_1 = Constraint(rule=_J_energy_balance_1)   

    def _J_energy_balance_2(m):
        return m.Q100 ==  m.UA*(m.T100 - m.T2)
    m.J_energy_balances_2 = Constraint(rule=_J_energy_balance_2)  

    def _J_energy_balance_3(m):
        return m.UA ==  0.16*( m.F3)
    m.J_energy_balances_3 = Constraint(rule=_J_energy_balance_3)  

    def _J_energy_balance_4(m):
        return m.F100 ==  m.Q100/m.lams
    m.J_energy_balances_4 = Constraint(rule=_J_energy_balance_4)  

    'Condenser Energy Balance'

    def _C_energy_balance_1(m):
        return m.Q200 == m.UA2*(m.T3 - m.T200)/(1 + (m.UA2/(2*m.Cp*m.F200)))
    m.C_energy_balances_1 = Constraint(rule=_C_energy_balance_1)   

    def _C_energy_balance_2(m):
        return m.F5 ==  m.Q200/m.lam
    m.C_energy_balances_2 = Constraint(rule=_C_energy_balance_2)  
# 
    'Overall level'

    def _L_balance(m):
        return m.F2 == m.F1 - m.F4
    m.L_balance_1 = Constraint(rule=_L_balance)   
             
    return m

def evaporator_plant(h, nfetp, ncpt): #full dynamic model to act as plant
    
    'Declare Model'
    
    m = ConcreteModel()
    m.dual = Suffix(direction=Suffix.IMPORT)    
    m.t = ContinuousSet(bounds = (0,h))

    'Declare Model Parameters'

    m.M = Param(initialize = 20, mutable = True)
    m.C = Param(initialize = 4, mutable = True)
    m.UA2 = Param(initialize = 6.84, mutable = True)
    m.Cp = Param(initialize = 0.07, mutable = True) 
    m.lam = Param(initialize = 38.5, mutable = True)
    m.lams  = Param(initialize = 36.6, mutable = True)      

    'Initial Conditions'

    m.X2init = Param(initialize = 25 , mutable = True)
    m.P2init = Param(initialize = 50.57000000697671, mutable = True)
    
    'Declare Model Variables'

    m.P2 = Var(m.t, initialize = 50.57) 
    m.P100 = Var(m.t, initialize = 193.45) 

    m.X1 = Var(m.t, initialize = 5) 
    m.X2 = Var(m.t, initialize = 25) 

    m.F1 = Var(m.t, initialize = 10)
    m.F2 = Var(m.t)    
    m.F3 = Var(m.t, initialize = 50)    
    m.F4 = Var(m.t)    
    m.F5 = Var(m.t)    
    m.F100 = Var(m.t)    
    m.F200 = Var(m.t, initialize = 207.33)    

    m.T1 = Var(m.t, initialize = 40)  
    m.T2 = Var(m.t)  
    m.T3 = Var(m.t)    
    m.T100 = Var(m.t) 
    m.T200 = Var(m.t, initialize = 25) 

    m.Q100 = Var(m.t)
    m.Q200 = Var(m.t)    
    m.UA = Var(m.t)
    
    'Declare state variables and discretization scheme'
    
    m.dX2dt = DerivativeVar(m.X2, wrt=m.t)
    m.dP2dt = DerivativeVar(m.P2, wrt=m.t)
    
    discretizer = TransformationFactory ('dae.collocation')
    discretizer.apply_to(m, nfe=nfetp, wrt=m.t, ncp=ncpt, scheme='LAGRANGE-RADAU')
    
    'Declare Model Equations'
    
    'Evaporator Mass Balances'
    
    def _material_balance_l(m,j):
        if j == 0:
            return Constraint.Skip
        return m.M*m.dX2dt[j] ==  m.F1[j]*m.X1[j] - m.F2[j]*m.X2[j]
    m.material_balance_l = Constraint(m.t, rule=_material_balance_l)   

    def _material_balance_l_init(m):
        return m.X2[0] ==  m.X2init
    m.material_balance_l_init = Constraint(rule=_material_balance_l_init) 
    
    def _material_balance_g(m,j):
        if j == 0:
            return Constraint.Skip
        return m.C*m.dP2dt[j] ==  m.F4[j] - m.F5[j]
    m.material_balance_g = Constraint(m.t, rule=_material_balance_g)  

    def _material_balance_g_init(m):
        return m.P2[0] ==  m.P2init
    m.material_balance_g_init = Constraint(rule=_material_balance_g_init) 
    
    'Evaporator Energy Balances'
    
    def _E_energy_balance_1(m,j):
        return m.T2[j] ==  0.5616*m.P2[j] + 0.3126*m.X2[j] + 48.43
    m.E_energy_balances_1 = Constraint(m.t, rule=_E_energy_balance_1)   

    def _E_energy_balance_2(m,j):
        return m.T3[j] ==  0.507*m.P2[j] + 55
    m.E_energy_balances_2 = Constraint(m.t, rule=_E_energy_balance_2)  

    def _E_energy_balance_3(m,j):
        return m.F4[j] ==  m.Q100[j]/m.lam - (m.F1[j]*m.Cp*(m.T2[j] - m.T1[j]))/m.lam
    m.E_energy_balances_3 = Constraint(m.t, rule=_E_energy_balance_3)  
    
    'Heat Jacket Energy Balance'

    def _J_energy_balance_1(m,j):
        return m.T100[j] ==  0.1538*m.P100[j] + 90
    m.J_energy_balances_1 = Constraint(m.t, rule=_J_energy_balance_1)   

    def _J_energy_balance_2(m,j):
        return m.Q100[j] ==  m.UA[j]*(m.T100[j] - m.T2[j])
    m.J_energy_balances_2 = Constraint(m.t, rule=_J_energy_balance_2)  

    def _J_energy_balance_3(m,j):
        return m.UA[j] ==  0.16*(m.F1[j] + m.F3[j])
    m.J_energy_balances_3 = Constraint(m.t, rule=_J_energy_balance_3)  

    def _J_energy_balance_4(m,j):
        return m.F100[j] ==  m.Q100[j]/m.lams
    m.J_energy_balances_4 = Constraint(m.t, rule=_J_energy_balance_4)  

    'Condenser Energy Balance'

    def _C_energy_balance_1(m,j):
        return m.Q200[j] == m.UA2*(m.T3[j] - m.T200[j])/(1 + m.UA2/(2*m.Cp*m.F200[j]))
    m.C_energy_balances_1 = Constraint(m.t, rule=_C_energy_balance_1)   

    def _C_energy_balance_2(m,j):
        return m.F5[j] ==  m.Q200[j]/m.lam
    m.C_energy_balances_2 = Constraint(m.t, rule=_C_energy_balance_2)  
 
    'Overall level'

    def _L_balance(m,j):
        return  0 == m.F1[j] - m.F4[j] - m.F2[j]
    m.L_balance_1 = Constraint(m.t, rule=_L_balance) 

    return m

def adjustment(dphidup, dgdup, gi, phii, bias, Uma):  #adjustment subproblem
    
    'Declare Model'
    
    m = ConcreteModel()
    m.dual = Suffix(direction=Suffix.IMPORT)    
    m.u = RangeSet(1,3)
    m.g = RangeSet(1,3)
    
    'Declare Model Parameters'

    m.pu = Param(initialize = 100, mutable = True)
    m.pl = Param(initialize = 40, mutable = True)
    m.gl = Param(initialize = 25, mutable = True)

    m.dgdu = Param(m.u, m.g, initialize = {(1,1):value(dgdup[0,0]) , (2,1):value(dgdup[1,0]) , (3,1):value(dgdup[2,0]) , (1,2):value(dgdup[0,1]) , (2,2):value(dgdup[1,1]) , (3,2):value(dgdup[2,1]) , (1,3):value(dgdup[0,2]) , (2,3):value(dgdup[1,2]) , (3,3):value(dgdup[2,2]) }, mutable = True)
    m.dphidu = Param(m.u, initialize = {1:value(dphidup[0,0]), 2:value(dphidup[1,0]), 3:value(dphidup[2,0])}, mutable = True)

    m.bias = Param(initialize = value(bias), mutable = True)
    
    'Initial Conditions'

    m.ginit = Param(m.g, initialize = {1:value(gi[0]), 2:value(gi[1]), 3:value(gi[2])} , mutable = True)
    
    'Declare Model Variables'

    m.delu = Var(m.u, initialize = 0) 
    m.ghat = Var(m.g) 
    
    'Evaporator Mass Balances'

    def _affine_law_g(m,i):
        return m.ghat[i] == m.ginit[i] + sum(m.dgdu[j,i]*m.delu[j] for j in sorted(m.u))
    m.affine_law_g = Constraint(m.g, rule=_affine_law_g)  

    def _p_l1(m):
        return m.ghat[1] <= 0
    m.p_l1 = Constraint(rule=_p_l1) 

    def _p_l2(m):
        return m.ghat[2] <= 0
    m.p_l2 = Constraint(rule=_p_l2) 

    def _p_l3(m):
        return m.ghat[3] <= 0
    m.p_l3 = Constraint(rule=_p_l3) 

    def _u1_l(m):
        return m.delu[1] >= -value(Uma[0,0])*0.05
    m.u1_l = Constraint(rule=_u1_l) 
    
    def _u1_u(m):
        return m.delu[1] <= value(Uma[0,0])*0.05
    m.u1_u = Constraint(rule=_u1_u) 
    
    def _u2_l(m):
        return m.delu[2] >= -value(Uma[1,0])*0.05
    m.u2_l = Constraint(rule=_u2_l) 
    
    def _u2_u(m):
        return m.delu[2] <= value(Uma[1,0])*0.05
    m.u2_u = Constraint(rule=_u2_u) 

    def _u3_l(m):
        return m.delu[3] >= -value(Uma[2,0])*0.05
    m.u3_l = Constraint(rule=_u3_l) 
    
    def _u3_u(m):
        return m.delu[3] <= value(Uma[2,0])*0.05
    m.u3_u = Constraint(rule=_u3_u) 
    
    m.obj = Objective(expr = (m.ghat[1]-m.bias)**2)
      
    return m

'Process'    
nstates = 2 #number of states
ninputs = 3 #number of inputs
adapt = [0,1,2] #all inputs are adapted since this is the full MA problem
ind1 = 0 #indicates that inputs were just changed
ind2 = 0 #indicates that disturbance just occurred

switch = [] #indicates inputs used for modification
pricespred = [] #stores modified cost

dis1 = 10 #nominal disturbance values
dis2 = 5
dis3 = 40
dis4 = 25

'Simulation'
nfetp = 1 #number of finite elements per time step for dynamic plant model
ncpt = 3 #number of collocation points per finite element for dynamic plant model
h = 1 #plant time step (min)
opw = 20000 #total number of time steps in given simulation
period = 2000 #disturbance period (time periods)
nadapt = 1 #number of inputs used for modification
pert =  0.001  #perturbations size

'Modifier adaptation'
toggle = 0 #indicates whether adjustment subproblem is active
toggle0 = 0 #indicates that MA problem was just executed
nadj = 0 #number of adjustments that have been made
li = 300 #settling time when initializing simulation (time periods) 
lio = 400 #settling time after set point change (time periods)
perti = np.zeros([ninputs,1])
sst = 300 #settling time after perturbation (time periods)
nconst = 3 #number of constraints
count = 0 #keeps track of how many perturbations have been performed

filtphi = [0.01,0.01,0.01] #cost gradient modifier filters
filtg = [0.01,0.01,0.01] #constraint gradient modifier filters
filte = [0.01,0.01,0.01] #constraint value modifier filters

t = [] #time vector

Urto = np.zeros([ninputs,1]) #current inputs for non-mismatched model
Urto[0,0] = 200 #nominal P100
Urto[1,0] = 200 #nominal F200
Urto[2,0] = 50 #nominal F3
Uma = np.zeros([ninputs,1]) #current inputs for mismatched model
Uma[0,0] = 200
Uma[1,0] = 200
Uma[2,0] = 50

'Arrays to store data'

x1 = np.array([[0] for i in range(0,nstates)], dtype='float64')
x2 = np.array([[0] for i in range(0,nstates)], dtype='float64')
u1 = np.array([[0] for i in range(0,ninputs)], dtype='float64')
u2 = np.array([[0] for i in range(0,ninputs)], dtype='float64')
econ1 = []
econ2 = []
R = np.array([[0] for i in range(0,ninputs)], dtype='float64')
Rphi = np.array([[0] for i in range(0,ninputs)], dtype='float64')

lamg = np.zeros([ninputs, nconst])
lamgp = np.zeros([ninputs, nconst])
lamphi = np.zeros([ninputs, 1])
lamphip = np.zeros([ninputs, 1])
lamphiperm = np.zeros([ninputs, 1])
lamphidummy= np.zeros([ninputs, 1])
epsg = np.zeros([nconst, 1])
epsgp = np.zeros([nconst, 1])
dgdup = np.zeros([ninputs, nconst])
dgdum = np.zeros([ninputs, nconst])
gpold = np.zeros([nconst, 1])
gmold = np.zeros([nconst, 1])
uk = np.zeros([ninputs,1])
gpnew = np.zeros([ninputs, nconst])
phinextp = np.zeros([ninputs,1])
phinextm = np.zeros([ninputs,1])
gmnew = np.zeros([ninputs, nconst])
dphidum = np.zeros([ninputs,1])
dphidup = np.zeros([ninputs,1])
grad = np.zeros([ninputs,1])
gradg = np.zeros([ninputs, nconst])

lambdaphit = np.array([[0] for i in range(0,ninputs)], dtype='float64')
lambdagt = np.array([[0] for i in range(0,nconst)], dtype='float64')
gradphit = np.array([[0] for i in range(0,ninputs)], dtype='float64')
gradgt = np.array([[0] for i in range(0,nconst)], dtype='float64')

'Prices'

P1 = 0.1009
P2 = 0
P3 = 60
P4 = 60
P5 = 0

'Warm-starting RTO and MA using feasibility problems'

rto = evaporator_RTO_no_mismatch(P1,P2,P3,P4)
rto.F1.fix(10)
rto.X1.fix(5)
rto.T1.fix(40)
rto.T200.fix(25)
rto.UA2 =  6.84
opt2 = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
rtoresults = opt2.solve(rto,tee=True)
rto.solutions.store_to(rtoresults)

ma = evaporator_RTO_mismatch(P1,P2,P3,P4, ninputs, epsg, lamg, lamphi, uk)
ma.F1.fix(10)
ma.X1.fix(5)
ma.T1.fix(40)
ma.T200.fix(25)
ma.UA2 =  6.84
opt2 = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
maresults = opt2.solve(ma,tee=True)
ma.solutions.store_to(maresults)

print('RTO')
print(value(rto.P100))
print(value(rto.F200))
print(value(rto.F3))
print(value(rto.P2))
print(value(rto.X2))
print(value(P1*(rto.F2 + rto.F3) + P2*rto.F100 + P3*rto.F200 + P4*rto.P100))
print('MA')
print(value(ma.P100))
print(value(ma.F200))
print(value(ma.F3))
print(value(ma.P2))
print(value(ma.X2))
print(value(P1*(ma.F2 + ma.F3) + P2*ma.F100 + P3*ma.F200 + P4*ma.P100))

plant1 = evaporator_plant(h, nfetp, ncpt) #name for plant 1 optimized with no mismatch
plant2 = evaporator_plant(h, nfetp, ncpt) #name for plant 2 optimized with mismatch

'Disturbance sequence used to compare different number of modifications and schemes'

disturbance1 = [10,
 9.709649744462643,
 8.252329792015885,
 11.870557001882391,
 9.664396466759055,
 8.759508181731238,
 10.968703850809442,
 9.727863441591841,
 11.948501872832576,
 11.153037560319408]
disturbance2 = [5,
 5.889434169324909,
 5.219155047603859,
 4.532121827330779,
 4.567890955305049,
 4.244839162842458,
 4.389038454623736,
 5.722816941794552,
 5.8574358733039915,
 5.285174966961637]
disturbance3 = [40,
 43.306040788061935,
 44.857292315231135,
 34.44142290537077,
 36.763343916143384,
 39.837713496768906,
 47.75440994592864,
 33.56402732428344,
 33.188686539295745,
 47.80434348003715]
disturbance4 = [25,
 25.230617034104874,
 27.79735651075671,
 22.96289429727007,
 27.14105720833182,
 23.145133757526064,
 23.58630187231373,
 28.68943237150072,
 29.108082678140057,
 24.3303742604355]


for l in range(0,opw):  #loop for opw time periods

    if l == li-100 and value(x2[0,-2]) < 25 and toggle0 == 1: #check for constraint vaiolations and activate adjustment subproblem
        toggle = 1
        

    if (l/period).is_integer() == True and l > 0: # if disturbance has just occured
        li = l + 50
        for i in adapt:
            lamphip[i,0] = value(0)
            grad[i,0] = value(0)
            dphidup[i,0] = value(0)
            dphidum[i,0] = value(0)
            for j in range(0,len(lamgp[0])):
                lamgp[i,j] = value(0)
                dgdup[i,j] = 0
                dgdum[i,j] = 0
        lamphiperm = np.zeros([ninputs, 1])
        adapt = [0,1,2]
        count = 0
        li = l + lio        
        ind1 = 0
        ind2 = 1
        toggle = 0
        toggle0 = 0
#        dis1 = 10*np.random.uniform (0.8,1.2) # can generate random disturbances if so desired by sampling from this distribution
#        dis2 = 5*np.random.uniform (0.8,1.2)
#        dis3 = 40*np.random.uniform (0.8,1.2)
#        dis4 = 25*np.random.uniform (0.8,1.2)        
        dis1 = disturbance1[int(l/period)-1]
        dis2 = disturbance2[int(l/period)-1]
        dis3 = disturbance3[int(l/period)-1]
        dis4 = disturbance4[int(l/period)-1]
        
        plant1 = evaporator_plant(h, nfetp, ncpt)
        plant2 = evaporator_plant(h, nfetp, ncpt)   
    else:
        pass  
        
    if l == 0: #setting nominal input values and initial conditions at t == 0
        plant1.X2init = value(rto.X2)
        plant1.P2init = value(rto.P2)
        plant1.P100.fix(value(rto.P100))
        plant1.F200.fix(value(rto.F200))
        plant1.F3.fix(value(rto.F3))
        plant1.F1.fix(dis1)
        plant1.X1.fix(dis2)
        plant1.T1.fix(dis3)
        plant1.T200.fix(dis4)
        
        plant2.X2init = value(rto.X2)
        plant2.P2init = value(rto.P2)
        plant2.P100.fix(value(rto.P100))
        plant2.F200.fix(value(rto.F200))
        plant2.F3.fix(value(rto.F3))
        plant2.F1.fix(dis1)
        plant2.X1.fix(dis2)
        plant2.T1.fix(dis3)
        plant2.T200.fix(dis4)
        
    else: #setting updated input values and initial conditions  thereafter
    
        plant1.X2init = value(x1[0,-2])
        plant1.P2init = value(x1[1,-2])
        plant1.P100.fix(value(Urto[0,0]))
        plant1.F200.fix(value(Urto[1,0]))
        plant1.F3.fix(value(Urto[2,0]))
        plant1.F1.fix(dis1)
        plant1.X1.fix(dis2)
        plant1.T1.fix(dis3)
        plant1.T200.fix(dis4)

        plant2.X2init = value(x2[0,-2])
        plant2.P2init = value(x2[1,-2])
        plant2.F1.fix(dis1)
        plant2.X1.fix(dis2)
        plant2.T1.fix(dis3)
        plant2.T200.fix(dis4)

        if toggle == 0 :               
            if l == (li + sst*count + 1) and l < (li + sst*len(adapt)):  #indicates which input is being perturbed
                for i in range(0,ninputs):
                    if i == adapt[count]:
                        perti[i] = value(pert)
                    else:
                        perti[i] = value(0)
    
            if count > len(adapt)-1:
                plant2.P100.fix(value(Uma[0,0]))
                plant2.F200.fix(value(Uma[1,0]))
                plant2.F3.fix(value(Uma[2,0]))                 
                count = 0 
                for i in range(0,ninputs):
                    perti[i] = value(0)
                      
            elif l > (li + sst*(count)) and l < (li + sst*(count+1)):
                plant2.P100.fix(value(Uma[0,0]+perti[0,0]*Uma[0,0]))
                plant2.F200.fix(value(Uma[1,0]+perti[1,0]*Uma[1,0]))                    
                plant2.F3.fix(value(Uma[2,0]+perti[2,0]*Uma[2,0]))                    
            elif l == (li + sst*(count+1)):
                count += 1
            else:
                plant2.P100.fix(value(Uma[0,0]))
                plant2.F200.fix(value(Uma[1,0]))
                plant2.F3.fix(value(Uma[2,0]))  
                
        elif toggle == 1 and (l/20).is_integer() == True: #solves adjustment subproblem
            gi = [value(25-x2[0,-2]),value(40-x2[1,-2]),value(x2[1,-2]-100)]
            phii = value(econ2[-2])
            bias = 0
            adj = adjustment(dphidup, dgdup, gi, phii, bias, Uma)
            opt4 = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
            adjresults = opt4.solve(adj,tee=True)
            adj.solutions.store_to(adjresults)
            Uma[0,0] = value(Uma[0,0] + adj.delu[1])
            Uma[1,0] = value(Uma[1,0] + adj.delu[2])
            Uma[2,0] = value(Uma[2,0] + adj.delu[3])
            plant2.P100.fix(value(Uma[0,0]))
            plant2.F200.fix(value(Uma[1,0]))
            plant2.F3.fix(value(Uma[2,0]))
            li += 20
            nadj += 1
            if abs(gi[0]-25) < 0.01 or nadj > 5:
                toggle = 0
                toggle0 = 0
                li += 20
                nadj = 0

        else:
            plant2.P100.fix(value(Uma[0,0]))
            plant2.F200.fix(value(Uma[1,0]))
            plant2.F3.fix(value(Uma[2,0]))            
                     
    opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt") # simulating plant 1 as dynamic feasibility problem
    opt.options['linear_solver'] = 'ma27'
    plantresults1 = opt.solve(plant1,tee=True)
    plant1.solutions.store_to(plantresults1)
    if plantresults1.solver.termination_condition != TerminationCondition.optimal:
        break

    opt = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt") # simulating plant 2 as dynamic feasibility problem
    opt.options['linear_solver'] = 'ma27'
    plantresults2 = opt.solve(plant2,tee=True)
    plant2.solutions.store_to(plantresults2)
    if plantresults2.solver.termination_condition != TerminationCondition.optimal:
        break
    
    if l == li + len(adapt)*sst + 1: #solving non-mismatched RTO at every disturbance
        rto = evaporator_RTO_no_mismatch(P1,P2,P3,P4)
        rto.F1.fix(dis1)
        rto.X1.fix(dis2)
        rto.T1.fix(dis3)
        rto.T200.fix(dis4)
        rto.UA2 =  6.84
        opt2 = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
        rtoresults = opt2.solve(rto,tee=True)
        rto.solutions.store_to(rtoresults)
        Urto[0] = value(rto.P100)
        Urto[1] = value(rto.F200)        
        Urto[2] = value(rto.F3)        

    if l == li + len(adapt)*sst + 1:  #updating modified cost and input used for modification
        for i in range(0,len(lamphi)):
            lamphi[i] = value((1-filtphi[i])*grad[i] + filtphi[i]*lamphip[i])
        for i in range(0,len(lamg)):
            for j in range(0,len(lamg[0])):
                lamg[i,j] = value((1-filtg[i])*(dgdup[i,j] - dgdum[i,j]) + filtg[i]*lamgp[i,j])
        for i in range(0,len(epsg)):
            epsg[i] = value((1-filte[0])*(gpold[i] - gmold[i]) + filte[0]*epsgp[i])

        for i in range(0,len(lamphi)):
            lamphip[i] = value(lamphi[i])
        for i in range(0,len(lamg)):
            for j in range(0,len(lamg[0])):
                lamgp[i,j] = value(lamg[i,j])
        for i in range(0,len(epsg)):
            epsgp[i] = value(epsg[i])                

        for i in range(0,len(uk)):
            uk[i] = value(Uma[i,0])
        for i in range(0,len(adapt)):
            lamphiperm[adapt[i]] = value(lamphi[adapt[i]]) 
        for i in range(0,len(lamphi)):
            lamphidummy[i,0] = value(lamphiperm[i,0])            
            ma = evaporator_RTO_mismatch_eval(P1,P2,P3,P4, ninputs, epsg, lamg, lamphi, uk)
            ma.F1.fix(dis1)
            ma.X1.fix(dis2)
            ma.T1.fix(dis3)
            ma.T200.fix(dis4)
            ma.P100.fix(value(uk[0,0]))
            ma.F200.fix(value(uk[1,0]))
            ma.F3.fix(value(uk[2,0]))
            ma.UA2 =  6.84
            opt2 = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
            maresults = opt2.solve(ma,tee=True)
            ma.solutions.store_to(maresults)
        
            pricespred.append(value(P1*(ma.F2 + ma.F3) + P2*ma.F100 + P3*ma.F200 + P4*ma.P100 + float(lamphidummy[0,0])*ma.P100 + float(lamphidummy[1,0])*ma.F200 + float(lamphidummy[2,0])*ma.F3))                
            lamphidummy = np.zeros([ninputs, 1])


        switch.append(sorted([pricespred.index(sorted(pricespred)[0])])) 
        adapt = sorted([pricespred.index(sorted(pricespred)[0])])
        
        for i in range(0,len(lamphi)):
            if i == adapt[0]:
                pass
            else: 
                grad[i,0] = 0
                dphidum[i,0] = 0
                dphidup[i,0] = 0
                lamphip[i,0] = 0
                for j in range(0,len(lamg[0])):
                    lamgp[i,j] = 0
                    dgdup[i,j] = 0
                    dgdum[i,j] = 0

        for i in range(0,len(lamphi)): #updating modifiers
            lamphi[i] = value((1-filtphi[i])*grad[i] + filtphi[i]*lamphip[i])
        for i in range(0,len(lamg)):
            for j in range(0,len(lamg[0])):
                lamg[i,j] = value((1-filtg[i])*(dgdup[i,j] - dgdum[i,j]) + filtg[i]*lamgp[i,j])
        for i in range(0,len(epsg)):
            epsg[i] = value((1-filte[0])*(gpold[i] - gmold[i]) + filte[0]*epsgp[i])

        for i in range(0,len(lamphi)):
            lamphip[i] = value(lamphi[i])
        for i in range(0,len(lamg)):
            for j in range(0,len(lamg[0])):
                lamgp[i,j] = value(lamg[i,j])
        for i in range(0,len(epsg)):
            epsgp[i] = value(epsg[i])                

        for i in range(0,len(uk)):
            uk[i] = value(Uma[i,0])

        ma = evaporator_RTO_mismatch(P1,P2,P3,P4, ninputs, epsg, lamg, lamphi, uk)
        ma.F1.fix(dis1)
        ma.X1.fix(dis2)
        ma.T1.fix(dis3)
        ma.T200.fix(dis4)
        ma.UA2 =  6.84
        opt2 = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt") #Solving dMAIS problem at every refinement step
        maresults = opt2.solve(ma,tee=True)
        ma.solutions.store_to(maresults)
        cost1 = value(P1*(ma.F2 + ma.F3) + P2*ma.F100 + P3*ma.F200 + P4*ma.P100)
        if (maresults.solver.termination_condition == TerminationCondition.optimal):
            Uma[0,0] = value(ma.P100)
            Uma[1,0] = value(ma.F200)
            Uma[2,0] = value(ma.F3)
            toggle0 = 1

        lamg = np.zeros([ninputs, nconst])
        lamphi = np.zeros([ninputs, 1])
        epsg = np.zeros([nconst, 1])
        pricespred = []     
        
        li = l + lio
        ind1 = 1
        
    if l == li - 1: #saving current mismatched model cost prediction
        phipold = value(econ2[-1])
        gpold[0,0] = value(25 - x2[0,-2])
        gpold[1,0] = value(40 - x2[1,-2])
        gpold[2,0] = value(x2[1,-2] - 100) 
        ma = evaporator_RTO_mismatch_eval(P1,P2,P3,P4, ninputs, epsg, lamg, lamphi, uk)
        ma.F1.fix(dis1)
        ma.X1.fix(dis2)
        ma.T1.fix(dis3)
        ma.T200.fix(dis4)
        ma.P100.fix(value(Uma[0,0]))
        ma.F200.fix(value(Uma[1,0]))
        ma.F3.fix(value(Uma[2,0]))
        ma.UA2 =  6.84
        opt2 = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
        maresults = opt2.solve(ma,tee=True)
        ma.solutions.store_to(maresults)
        if (maresults.solver.termination_condition == TerminationCondition.optimal):
            pass
        else:
            break
        phimold = value(P1*(ma.F2 + ma.F3) + P2*ma.F100 + P3*ma.F200 + P4*ma.P100)      
        gmold[0,0] = value(25 - ma.X2)
        gmold[1,0] = value(40 - ma.P2)
        gmold[2,0] = value(ma.P2 - 100)  

    elif (li <= l) and (l <= li +sst*len(adapt)): #performing perturbations on mismatched model

        if count > len(adapt)-1:
            pass                 
        elif l == (li + sst*(count+1) - 2):
            gpnew[adapt[count],0] = value(25 - plant2.X2[h])
            gpnew[adapt[count],1] = value(40 - plant2.P2[h])
            gpnew[adapt[count],2] = value(plant2.P2[h] - 100)   
            phinextp[adapt[count]] =value(P1*(plant2.F2[h] + plant2.F3[h]) + P2*plant2.F100[h] + P3*plant2.F200[h] + P4*plant2.P100[h])  
            ma = evaporator_RTO_mismatch_eval(P1,P2,P3,P4, ninputs, epsg, lamg, lamphi, uk)
            ma.F1.fix(dis1)
            ma.X1.fix(dis2)
            ma.T1.fix(dis3)
            ma.T200.fix(dis4)
            ma.P100.fix(value(plant2.P100[h]))
            ma.F200.fix(value(plant2.F200[h]))
            ma.F3.fix(value(plant2.F3[h]))
            ma.UA2 = 6.84
            opt2 = SolverFactory("ipopt", executable="C:\software\cygwin\home\ipopt")
            maresults = opt2.solve(ma,tee=True)
            ma.solutions.store_to(maresults)
            if (maresults.solver.termination_condition == TerminationCondition.optimal):
                pass
            else:
                break
            phinextm[adapt[count],0] = value(P1*(ma.F2 + ma.F3) + P2*ma.F100 + P3*ma.F200 + P4*ma.P100)   
            gmnew[adapt[count],0] = value(25 - ma.X2)
            gmnew[adapt[count],1] = value(40 - ma.P2)
            gmnew[adapt[count],2] = value(ma.P2 - 100) 
            
            dphidum[adapt[count],0] = value((phinextm[adapt[count],0]-phimold)/(pert*Uma[adapt[count],0]))
            dphidup[adapt[count],0] = value((phinextp[adapt[count],0]-phipold)/(pert*Uma[adapt[count],0]))
            for i in range(0,nconst):
                dgdum[adapt[count],i] = value((gmnew[adapt[count],i]-gmold[i,0])/(pert*Uma[adapt[count],0]))
                dgdup[adapt[count],i] = value((gpnew[adapt[count],i]-gpold[i,0])/(pert*Uma[adapt[count],0])) 
            grad[adapt[count],0] = value(dphidup[adapt[count],0]-dphidum[adapt[count],0])
            for i in range(0,nconst):
                gradg[adapt[count],i] = value(dgdup[adapt[count],i]-dgdum[adapt[count],i])
            
            v = []
            for i in range(0,ninputs):
                v.append(value(lamphip[i,0]))
            lambdaphit = np.insert(lambdaphit, -1, v, 1)
            v = []
            for i in range(0,ninputs):
                v.append(value(grad[i,0]))
            gradphit = np.insert(gradphit, -1, v, 1)         
        else:
            pass
                                  
    else:
        pass
    
    if l == 0: #saving data to arrays created above
        for j in sorted(plant1.t):
            t.append(l*h + j)
            econ1.append(value(P1*(plant1.F2[j] + plant1.F3[j]) + P2*plant1.F100[j] + P3*plant1.F200[j] + P4*plant1.P100[j]))
            v = []
            v.append(value(plant1.X2[j]))
            v.append(value(plant1.P2[j]))            
            x1 = np.insert(x1, -1, v, 1)
            v = []                
            v.append(value(plant1.P100[h]))
            v.append(value(plant1.F200[h]))            
            v.append(value(plant1.F3[h]))            
            u1 = np.insert(u1, -1, v, 1)
            
            econ2.append(value(P1*(plant2.F2[j] + plant2.F3[j]) + P2*plant2.F100[j] + P3*plant2.F200[j] + P4*plant2.P100[j]))
            v = []
            v.append(value(plant2.X2[j]))
            v.append(value(plant2.P2[j]))   
            x2 = np.insert(x2, -1, v, 1)
            v = []
            v.append(value(plant2.P100[h]))
            v.append(value(plant2.F200[h]))            
            v.append(value(plant2.F3[h]))  
            u2 = np.insert(u2, -1, v, 1)
            v = []
            for i in range(0,ninputs):
                v.append(value(abs(lamphip[i]))[0])
            R = np.insert(R, -1, v, 1)           
            v = []
            for i in range(0,ninputs):
                v.append(value((abs(lamphip[i])))[0])           
            Rphi = np.insert(Rphi, -1, v, 1)  
    else:
        for j in sorted(plant1.t)[1:]:
            t.append(l*h + j)
            econ1.append(value(P1*(plant1.F2[j] + plant1.F3[j]) + P2*plant1.F100[j] + P3*plant1.F200[j] + P4*plant1.P100[j]))
            v = []
            v.append(value(plant1.X2[j]))
            v.append(value(plant1.P2[j]))            
            x1 = np.insert(x1, -1, v, 1)
            v = []
            v.append(value(plant1.P100[h]))
            v.append(value(plant1.F200[h]))            
            v.append(value(plant1.F3[h])) 
            u1 = np.insert(u1, -1, v, 1)
            
            econ2.append(value(P1*(plant2.F2[j] + plant2.F3[j]) + P2*plant2.F100[j] + P3*plant2.F200[j] + P4*plant2.P100[j]))
            v = []
            v.append(value(plant2.X2[j]))
            v.append(value(plant2.P2[j]))   
            x2 = np.insert(x2, -1, v, 1)
            v = []
            v.append(value(plant2.P100[h]))
            v.append(value(plant2.F200[h]))            
            v.append(value(plant2.F3[h])) 
            u2 = np.insert(u2, -1, v, 1)
            v = []
            for i in range(0,ninputs):
                v.append(value(abs(lamphip[i]))[0])
            R = np.insert(R, -1, v, 1)           
            v = []
            for i in range(0,ninputs):
                v.append(value((abs(lamphip[i])))[0])           
            Rphi = np.insert(Rphi, -1, v, 1)  

    print(l)

fig = plt.figure(5) #plotting X2
plt.plot(t,x1[0][:-1], label = 'perfect model')
plt.plot(t,x2[0][:-1], label = 'dMAIS')
plt.title(r'Composition')
plt.legend(loc='upper left',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

fig = plt.figure(5) #plotting P2
plt.plot(t,x1[1][:-1], label = str(i))
plt.plot(t,x2[1][:-1], label = str(i))
plt.title(r'Pressure')
plt.legend(loc='upper left',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

fig = plt.figure(5) #plotting P100
plt.plot(t,u1[0][:-1], label = str(i))
plt.plot(t,u2[0][:-1], label = str(i))
plt.title(r'Pressure')
plt.legend(loc='upper left',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

fig = plt.figure(5) #plotting F200
plt.plot(t,u1[1][:-1], label = str(i))
plt.plot(t,u2[1][:-1], label = str(i))
plt.title(r'Flowrate')
plt.legend(loc='upper left',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

fig = plt.figure(5) #plotting F3
plt.plot(t,u1[2][:-1], label = str(i))
plt.plot(t,u2[2][:-1], label = str(i))
plt.title(r'Flowrate')
plt.legend(loc='upper left',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

fig = plt.figure(5) #plotting modifiers
plt.plot(t,R[0][:-1], label = str(i))
plt.legend(loc='upper left',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

fig = plt.figure(5) #plotting cost
plt.plot(t,econ1, label = 'Pefect model')
plt.plot(t,econ2, label = 'dMAIS')
plt.title(r'Process cost')
plt.legend(loc='upper left',fancybox=True,shadow=True)
plt.ylabel('Value')
plt.xlabel('min')
plt.show()

